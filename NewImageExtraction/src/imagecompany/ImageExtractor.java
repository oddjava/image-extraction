package imagecompany;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.FileUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class ImageExtractor 
{
	// References related to Mongo DB.
	MongoClient mongoClient;
	DB company;
	DBCollection selectedCollection;
	
	//Set Timer
	static long startTime = System.currentTimeMillis();
	
	//Constructor to establish DB Connection 
	ImageExtractor(String ip)
	{
		mongoClient = new MongoClient(ip, 27017);
		company = mongoClient.getDB("company");
		selectedCollection=company.getCollection("parsedDataForVPS");	//ForVPS	
	}
	
	// Get document from DB.
	void getImage(int startid,int endid)
	{			
		
				
		DBCursor cursor = selectedCollection.find(new BasicDBObject("_id", new BasicDBObject("$lte", endid).append("$gte", startid)).append("sellerStatus" , "validDomain").append("imageStatus" , "false"));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		int cursorCount=cursor.count();
		System.out.println("Total Count="+cursorCount);
			
		// Create local directory with name Images 		
		 File file = new File(MyRunnable.folderPath);
	     if (!file.exists()) 
	     {
	    	 if (file.mkdir()) 
	    	 {
	            System.out.println("Directory is created!");
	         } 
	    	 else 
	    	 {
	            System.out.println("Failed to create directory!");
	         }
	     }
	    	     
	    DBObject present=null; 
	    double loads[];
		while (cursor.hasNext()) 
		{
			loads=getLoadAvg();
			if(loads[0]<1.50) // Check load Average < 1.5 
			{	
				present=cursor.next();				
				new MyRunnable(present.get("_id").toString(),present.get("link").toString(),present.get("domainLink").toString(),selectedCollection).run();					
		
			}
			else // When cpu load average exceed 1.5 code will get terminate.
			{
				System.out.println("So tired. I'm Shuting Nowwwwwww........"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
				System.exit(123);	// return status 123 to shell script indicating abnormal termination.			
			}
		}		
		long endTime = System.currentTimeMillis();
		long minutes = TimeUnit.MILLISECONDS.toMinutes(endTime - startTime);
		System.out.println("Total Count="+cursorCount);
		System.out.println("It tooks " + minutes + " min");

		// Delete Images directory after code is finish
		try 
		{
			FileUtils.deleteDirectory(file);
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	// This method will get current load average of CPU using "uptime" command.
	double[] getLoadAvg()
	{
		StringBuffer output = new StringBuffer();
		double loads[]=new double[3];
        Process p;
        try {
                p = Runtime.getRuntime().exec("uptime");	// execute "uptime" command on linux only.
                p.waitFor();
                BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
                while ((line = reader.readLine())!= null) {
                     output.append(line + "\n"); //Output of command
                }

        } catch (Exception e) 
        {
                e.printStackTrace();
        }
        String str=output.toString();
        str=str.split("load average[s:][: ]")[1];  //To get only load avg from command output
        System.out.println("Load Average= "+str);
        String allValues[]=str.split(",");
        loads[0]=Double.parseDouble(allValues[0].trim()); //last one Min
        loads[1]=Double.parseDouble(allValues[1].trim()); //last five Min
        loads[2]=Double.parseDouble(allValues[2].trim()); //last fifteen Min
       // System.out.println(lastOneMin+"**"+lastFiveMin+"**"+lastFifteenMin+"**");
        return loads;
	}

	//Main Method
	public static void main(String args[]) throws Exception 
	{	
		new ImageExtractor(args[0]).getImage(Integer.parseInt(args[1]),Integer.parseInt(args[2]));
		System.exit(0);	// return status 0 to shell script indicating normal termination.
	}
}	

class MyRunnable
{
	final DBCollection selectedCollection;
	static final String folderPath = "./Images";

	// Create a trust manager that does not validate certificate chains like the 
		static TrustManager[] trustAllCerts = new TrustManager[] 
				 {
		    new X509TrustManager() {
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		            return null;
		        }
		        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
		            //No need to implement. 
		        }
		        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
		            //No need to implement. 
		        }
		    }
		};
		
	// To handle ssl handshake exception
		static
		{
			try {
			    SSLContext sc = SSLContext.getInstance("SSL");
			    sc.init(null, trustAllCerts, new java.security.SecureRandom());
			    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			} catch (Exception e) {
			    System.out.println(e);
			}
		}
	
	String id;	
	String link;
	String domainLink;
	
	MyRunnable(String id,String link,String domainLink,DBCollection selectedCollection) 
	{
		this.id=id;
		this.link=link;
		this.domainLink=domainLink;
		this.selectedCollection=selectedCollection;
	}
		
	void run() 
	{	
			//Set domainlink as filename. replace dots by _ 
			String fileName=domainLink.replace(".", "_").replace("/", "_");
			String src="";
			String format="";
			int width=0;
			
			try 
			{
				// call icons.better-idea.org API to get favicon or icon
				URL url = new URL("https://icons.better-idea.org/allicons.json?pretty=true&url="+link);
				HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
				
				// Get JSON as output and parse it
				JsonParser jp = new JsonParser(); 
				//Convert the input stream to a JSON element
				JsonElement root = jp.parse(new InputStreamReader((InputStream) conn.getContent())); 
				
				/* Sample JSON output
				 * {"url":"github.com","icons":[{"url":"https://github.com/apple-touch-icon-180x180.png","width":180,"height":180,"format":"png","bytes":21128,"error":null,"sha1sum":"7c7c91bf718078f67c641f81aa6929b58d0d2e52"}]}
				 * */
				JsonObject rootobj = root.getAsJsonObject();  
				
				// Get links in icons array
				JsonArray arr=rootobj.get("icons").getAsJsonArray();
			
				String temp1,temp2;
				for(int i=0;i<arr.size();i++)
				{	
					src=arr.get(i).getAsJsonObject().get("url").toString().replace("\"", "");
					
					// check whether favicon url is input url using host name of both url
					temp1=new URL(link).getHost().replace("www.", "");
					temp2=new URL(src).getHost().replace("www.", "");
							
					if(temp1.equalsIgnoreCase(temp2)) // If same
					{
						// Take url and terminate loop
						format=arr.get(0).getAsJsonObject().get("format").toString().replace("\"", "");   			    
						width=Integer.parseInt(arr.get(0).getAsJsonObject().get("width").toString().replace("\"", ""));
						break;
					}
					else
					{
						continue;
					}
				}					
				
				if(width>=30) // if image width >30
				{	
					fileName+="."+format; //update filename with extension
					url = new URL(src);
					downloadFile(link,url,fileName); //download file to local system.
				}
				else  // icon with initial letter of company name
				{
					fileName+=".png";
					url = new URL("https://icons.better-idea.org/icon?url="+link+"&size=120");
					downloadFile(link,url,fileName);	
				}	
			}
			catch (Exception e) // if JSON not found
			{			
				fileName+=".png";
				URL urlTemp;
				try 
				{
					urlTemp = new URL("https://icons.better-idea.org/icon?url="+link+"&size=120");
					downloadFile(link,urlTemp,fileName);
				}
				catch (MalformedURLException e1) 
				{
					e1.printStackTrace();
				}				
			}
		}
	
	// This method will download image to local system.
	void downloadFile(String link,URL url,String fileName)
	{
		File file= new File("./Images/" + fileName);
		OutputStream outStream=null;
		InputStream inStream=null;
		try 
		{			
			inStream = url.openStream();
			outStream = new FileOutputStream(file);
			byte[] bytes = new byte[2048];
		    int length;				    

		    while ((length = inStream.read(bytes)) != -1) {
		    	outStream.write(bytes, 0, length);
		    }	
		    new S3upload(file,fileName);  // Save local file to s3 bucket
		    selectedCollection.update(new BasicDBObject("link",link),new BasicDBObject("$set",new BasicDBObject("image" , fileName).append("imageStatus" , "true")));
		    System.out.println("Link= "+link+"\nName of a file= " +fileName+"\nFile has been saved\n\n");
			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			try 
			{
				inStream.close();
				outStream.close();
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
			}
		}
		
	}
}



